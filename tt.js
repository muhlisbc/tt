const { WebcastPushConnection } = require('tiktok-live-connector');

const tiktokUsername = process.argv[2].toLowerCase();

console.log(`User: @${tiktokUsername}`);

const client = new WebcastPushConnection(tiktokUsername);
const { ttsEvents, ttsConnect, ttsSpeak, playAudio, isTtsRunning } = require("./speech.js");


const MAX_QUE_LEN = 10;

const QUE = {
    chat: [],
    follow: [],
    join: [],
    gift: []
}

const TPL = {
    follow: [
        "{{user}}, makasih udah follow"
    ],
    join: [
        "{{user}}, makasih udah gabung",
        "{{user}}, makasih udah mampir",
        "{{user}}, makasih udah join"
    ],
    gift: [
        "{{user}}, makasih udah kasih {{count}} {{giftName}}",
        "{{user}}, makasih buat {{count}} {{giftName}} nya"
    ]
};

function addToQue(que, item) {
    if (que == "chat") {
        let val = QUE.chat;
        val = [...new Set(val)]

        QUE.chat = val;
    }

    QUE[que].push(item);

    QUE[que] = QUE[que].slice(-MAX_QUE_LEN);

    // console.log(`TTSrunning: ${isTtsRunning()}`)

    if (!isTtsRunning()) {
        ttsEvents.emit("end");
    }
}


client.on('chat', data => {
    if (!QUE.chat.includes(data.comment)) {
        addToQue("chat", data.comment);
    }
});

client.on("social", (data) => {
    if (data.displayType.match("_follow_")) {
        const text = randomItem(TPL.follow).replace("{{user}}", data.nickname);

        addToQue("follow", text);
    }
});


client.on("member", (data) => {
    // const text = randomItem(TPL.join).replace("{{user}}", data.nickname);

    // addToQue("join", text);
});

client.on('gift', data => {
    if (data.giftType === 1 && !data.repeatEnd) {
        //
    } else {
        const text = randomItem(TPL.gift)
            .replace("{{user}}", data.nickname)
            .replace("{{count}}", data.repeatCount)
            .replace("{{giftName}}", data.giftName);

        const coinCount = data.repeatCount * data.diamondCount;

        addToQue("gift", [coinCount, text]);
    }
});

client.on('streamEnd', () => {
    process.exit(1);
});

let lastSpeak = new Date();

ttsEvents.on("end", () => {
    const que = Object.keys(QUE).map((key) => {
        return `${key} ${QUE[key].length}`;
    }).join(" | ");

    console.log(`[QUE] ${que}`);

    if (isTtsRunning()) {
        ttsEvents.emit("end");

        return;
    };

    lastSpeak = new Date();

    if (QUE.gift.length > 0) {
        const item = QUE.gift.pop();

        let audio = "kiss.m4a";

        if (item[0] >= 50) {
            audio = "cheer.m4a";
        }

        playAudio(audio, () => {
            ttsSpeak(item[1], "wanita");
        });

        return;
    }

    if (QUE.chat.length > 0) {
        const item = QUE.chat.pop();

        ttsSpeak(item, "pria");

        return;

    }

    if (QUE.follow.length > 0) {
        const item = QUE.follow.pop();

        ttsSpeak(item, "wanita");

        return;

    }

    if (QUE.join.length > 0) {
        const item = QUE.join.pop();

        ttsSpeak(item, "wanita");

        return;
    }
});

function randomItem(items) {
    return items[Math.floor(Math.random()*items.length)];
}


client.connect().then(state => {
    console.log(`Connected to @${tiktokUsername} | roomId ${state.roomId}`);

    ttsConnect();

    // const randSpeech = [
    //     "ehem ehem",
    //     "ada curut lewat",
    //     "pada diem diem bae",
    //     "komen ges",
    //     "yang belum follow, jangan lupa follow",
    //     "yang ga komen belum mandi",
    //     "ada kucing kawin",
    //     "yang ga komen idaman polsek"
    // ];

    // setInterval(() => {
    //     if (((new Date()) - lastSpeak) > 15000) {
    //         lastSpeak = new Date();

    //         ttsEvents.emit("update-tts");

    //         ttsSpeak(randomItem(randSpeech), "wanita");
    //     }
    // }, 5000);
// }).catch(err => {
    // console.log(`Failed to connect: ${err}`);

    // process.exit(1);
})
