const WebSocketClient = require("websocket").client;
const fs = require('fs');
const { exec } = require('child_process');
const crypto = require("crypto");
const EventEmitter = require('events');

const GENDERS = {
    wanita: "Microsoft Server Speech Text to Speech Voice (id-ID, GadisNeural)",
    pria: "Microsoft Server Speech Text to Speech Voice (id-ID, ArdiNeural)"
}

const OUTFILE = "tts.mp3";

const client = new WebSocketClient({closeTimeout: 60000 * 5});
const ttsEvents = new EventEmitter();

client.on('connectFailed', (error) => {
    console.log('Connect Error: ' + error.toString());
});

let connection, ttsRunning;
// let liveEnded = false;

function speak(text, genderKey) {
    if (ttsRunning) {
        console.log(`tts ttsRunning ${ttsRunning}`)
        return;
    }

    text = text.replace(/[^0-9a-z\ \,\.\?\!]/gi, "").trim();

    if (text.length < 1) {
        ttsEvents.emit("end");

        return;
    }

    ttsRunning = true;
    const gender = GENDERS[genderKey];

    console.log(`[SPEAK] ${text} | ${genderKey}`);

    const reqID = crypto.randomBytes(16).toString('hex');
    
    connection.send(`X-RequestId:${reqID}\r\nContent-Type:application/ssml+xml\r\nPath:ssml\r\n\r\n<speak version='1.0' xmlns='http://www.w3.org/2001/10/synthesis' xml:lang='en-US'><voice  name='${gender}'><prosody pitch='+0Hz' rate ='+0%' volume='100'>${text}</prosody></voice></speak>`);
}

let binaryData = [];
let ttsConnected = false;

client.on("connect", (conn) => {
    // console.log("TTS connected");

    connection = conn;

    ttsConnected = true;

    console.log("WebSocket Client Connected");

    connection.on('error', (error) => {
        console.log("Connection Error: " + error.toString());
    });

    connection.on('close', () => {
        console.log('echo-protocol Connection Closed');
        console.log("Reconnecting to TTS server....");

        ttsConnect();
    });

    connection.on('message', (message) => {
        if (message.type == "utf8") {
            if (message.utf8Data.match("Path:turn.end")) {
                const binLength = binaryData.reduce((acc,element) => acc + element.length, 0);

                const bin = Buffer.concat(binaryData, binLength);

                fs.writeFile(OUTFILE, bin, () => {
                    ttsRunning = true;

                    playAudio(OUTFILE, () => {
                        binaryData = [];


                        fs.unlink(OUTFILE, () => {
                            ttsRunning = null;

                            ttsEvents.emit("end");
                        });

                    });
                });
            }
        } else if(message.type == "binary") {
            ttsRunning = true;
            
            binaryData.push(message.binaryData.slice(130));
        }
    });

    connection.send(`Content-Type:application/json; charset=utf-8\r\nPath:speech.config\r\n\r\n{"context": {"synthesis": {"audio": {"metadataoptions": {"sentenceBoundaryEnabled": "false", "wordBoundaryEnabled": "true"}, "outputFormat": "audio-24khz-48kbitrate-mono-mp3"}}}}`);
    ttsEvents.emit("end");
});

const ttsConnect = function() {
    const endPointParams = new URLSearchParams({"trustedclienttoken": "6A5AA1D4EAFF4E9FB37E23D68491D6F4"});
    const wsUrlWithParams = `wss://speech.platform.bing.com/consumer/speech/synthesize/readaloud/edge/v1?${endPointParams}`;

    client.connect(wsUrlWithParams);
}

ttsEvents.on("update-tts", () => {
    ttsRunning = false;
});


let playingAudio = false;

const playAudio = function(file, cb) {
    playingAudio = true;

    let cmd = `ffplay -nodisp -autoexit -nostats -hide_banner -loglevel quiet ${file}`;

    if (process.platform == "android") {
        cmd = `play-audio ${file}`;
    }

    console.log(`[AUDIO] ${file}`);

    exec(cmd, () => {
        playingAudio = false;

        cb();
    });
}

const isTtsRunning = function() {
    console.log(`[STAT] ttsRunning ${ttsRunning} | ttsConnected ${ttsConnected} | playingAudio ${playingAudio}`);

    return ttsRunning || !!!ttsConnected || playingAudio;
}

module.exports = {
    GENDERS,
    ttsEvents,
    ttsConnect: ttsConnect,
    ttsSpeak: speak,
    playAudio,
    isTtsRunning
}